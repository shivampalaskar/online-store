import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate{

  constructor(
    private router:Router
  ) { }
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    if( sessionStorage['role']){
      return true;
    }
    this.router.navigate(['/auth/login'])
    return false
  }

  logout(){
    sessionStorage.removeItem('role')
    this.router.navigate(['/auth/login'])
  }
}
