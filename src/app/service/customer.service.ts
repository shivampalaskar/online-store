import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  

  customer = {
    name : "Shivam Palaskar",
    address: "Aurangabad",
    mobNo: "8482923285"
  }
  constructor() { }

  updateCustomerInfo(customer: any) {
    this.customer = customer
  }
}
