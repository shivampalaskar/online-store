import { Injectable } from '@angular/core';
import { from } from 'rxjs';
import { Subject } from 'rxjs/internal/Subject';
//import { products } from '../global data/product'

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  products= [
    {
      "id":1,
      "category": "Home Appliances",
      "name": "Product-1",
      "price": 60
    },
    {
      "id":2,
      "category": "Electronics",
      "name": "Product-2",
      "price": 50
    }
  ]
  category = ["Electronics","Home Appliances","Cloths"]
  cart = []
  discount = 0
  
  constructor() { 
    localStorage.setItem("productList",JSON.stringify(this.products))
  }

  addProduct(product:any){
    let id = this.products.length+1
    product['id'] = id
    this.products.push(product)
    localStorage.setItem("productList",JSON.stringify(this.products))
  }

  updateProduct(product){
    let id = this.products.findIndex(e=>e['id'] == product['id'])
    this.products[id] = product
    localStorage.setItem("productList",JSON.stringify(this.products))
  }

  addToCart(product){
    let index = this.cart.findIndex(e => e.id==product['id'])
    if(index == -1){
      product['quantity'] = 1;
      this.cart.push(product)
    }else{
      this.cart[index]['quantity']++
    }
  }

  applyDiscount(discount){
    this.discount = discount
    localStorage.setItem("discount",JSON.stringify(this.discount))
  }

  removeProductFromCart(product){
    let index = this.cart.findIndex(e => e.id==product['id'])
    if(--this.cart[index]['quantity'] == 0){
      this.cart.splice(index,1)
    }
  }

  // Observable string sources
  private emitChangeSource = new Subject<any>();
  // Observable string streams
  changeEmitted$ = this.emitChangeSource.asObservable();
  // Service message commands
  emitChange(change: any) {
      this.emitChangeSource.next(change);
  }
}
