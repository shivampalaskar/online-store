import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  login: FormGroup;
  errorMsg : String

  constructor(
    private router:Router
  ) {
    this.login = new FormGroup({
      username: new FormControl('',[Validators.required, Validators.email]),
      password: new FormControl('',[Validators.required, Validators.minLength(6)])
    });
  }

  ngOnInit(): void {
  }

  onSubmit(){
    console.log(this.login.value)
    if( this.login.value['username'] == "admin@gmail.com" && this.login.value['password'] == "123456"){
      sessionStorage['role'] = "admin"
      this.router.navigate(["/admin"])  

    } else if( this.login.value['username'] == "customer@gmail.com" && this.login.value['password'] == "123456") {

      sessionStorage['role'] = "customer"
      this.router.navigate(["/customer"])

    } else {
      this.errorMsg = "Invalid Credentials"
    }
  }
}
