import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/service/product.service';
//import { products } from '../../global data/product'


@Component({
  selector: 'app-viewproduct',
  templateUrl: './viewproduct.component.html',
  styleUrls: ['./viewproduct.component.css']
})

export class ViewproductComponent implements AfterViewInit  {

  discountFormControl = new FormControl('',[Validators.pattern('^[1-9][0-9]?$|^100$')])
  displayedColumns: string[] = ['name', 'price','discprice', 'category','Action'];

  dataSource = new MatTableDataSource(this.productService.products)

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  constructor(
    private productService:ProductService,
    private router:Router
  ) { }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onUpdate(product){
    this.router.navigate(['admin/add-product', {productId: product["id"]}])
  }

  applyDiscount(){
    this.productService.applyDiscount(this.discountFormControl.value)
    this.discountFormControl.setValue('')
  }
}



