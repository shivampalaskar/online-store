import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService} from '../../service/product.service'
//import { products } from '../../global data/product'

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent implements OnInit {

  categoryList = []
  productId: any;

  constructor(
    private productService: ProductService,
    private router:Router,
    private activatedRoute: ActivatedRoute,
  ) {}

 
   product = new FormGroup({
    name: new FormControl("",[Validators.required]),
    category: new FormControl("",[Validators.required]),
    price: new FormControl("",[Validators.required])
  });

  ngOnInit(): void {
    this.categoryList = this.productService.category
    this.productId = this.activatedRoute.snapshot.params['productId'];
    let product = this.productService.products.find(e=>e['id'] == this.productId)
    this.product.patchValue(product) 
  }

  

  onSubmit(){
    if(this.productId = this.activatedRoute.snapshot.params['productId']){
      this.product.value['id'] = this.productId
      this.productService.updateProduct(this.product.value)
    }else{
      this.productService.addProduct(this.product.value)
    }
    this.productService.emitChange(this.productService.products)
    this.router.navigate(['/admin/view-product'])
  }
}
