import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddproductComponent } from './addproduct/addproduct.component';
import { AdminComponent } from './admin.component';
import { ViewproductComponent } from './viewproduct/viewproduct.component';

const routes: Routes = [
  { path: '', component: AdminComponent,
    children : [
      {path: 'add-product', component: AddproductComponent},
      {path: 'view-product', component: ViewproductComponent},
      {path: '', redirectTo: 'view-product', pathMatch: 'full'}
    ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
