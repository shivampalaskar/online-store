import { AfterViewInit, Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/service/product.service';
//import { products } from '../../global data/product'

@Component({
  selector: 'app-viewproduct',
  templateUrl: './viewproduct.component.html',
  styleUrls: ['./viewproduct.component.css']
})
export class ViewproductComponent implements AfterViewInit {
  
  displayedColumns: string[] = ['name', 'price', 'discprice','category','Action'];
  discount: number = JSON.parse(localStorage.getItem("discount"))
  dataSource = new MatTableDataSource(JSON.parse(localStorage.getItem("productList")))
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  constructor(
    private productService:ProductService,
    private router:Router
  ) { }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onAdd(product){
    this.productService.addToCart(product)
    this.productService.emitChange(this.productService.cart.map(e => e.quantity).reduce((total,curr) => total+curr))
  }
}
