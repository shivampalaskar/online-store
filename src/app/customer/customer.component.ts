import { Component, OnInit, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { ProductService } from '../service/product.service';
import { ViewproductComponent } from '../admin/viewproduct/viewproduct.component';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  cartItems
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private productService: ProductService,
    private authService: AuthService
    ) {
      this.productService.changeEmitted$.subscribe(data =>{
        this.cartItems = data
      })
    }

  ngOnInit(): void {
    this.cartItems = this.productService.cart.length
  }

  onLogout(){
    this.authService.logout()
  }
}
