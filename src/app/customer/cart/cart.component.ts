import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements AfterViewInit {

  cart = this.productService.cart
  discount: number = JSON.parse(localStorage.getItem("discount"))

  displayedColumns: string[] = ['name', 'quantity', 'price', 'action'];

  dataSource = new MatTableDataSource(this.productService.cart)

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  constructor(
    private productService:ProductService,
    private router:Router,
    private dialog: MatDialog
  ) {  }

  getTotalCost(){
    return this.cart.map(p => (p.price * (100 - this.discount) / 100) * (p.quantity)).reduce((acc, value) => acc + value, 0);
  }

  onRemove(product){
    this.productService.removeProductFromCart(product)
    this.dataSource = new MatTableDataSource(this.productService.cart)
    this.productService.emitChange(this.productService.cart.map(e => e.quantity).reduce((total,curr) => curr+total,0))
  }
  
  closeModal(){
    this.router.navigate(['customer/view-product'])
  }

}
