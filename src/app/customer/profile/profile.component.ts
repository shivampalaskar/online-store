import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/service/customer.service';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  edit: boolean = false
  customer
  constructor(
    private customerService:CustomerService
  ) { }

  ngOnInit(): void {
    this.customer = this.customerService.customer
  }

  onEdit(){
    this.edit = true;
  }

  onUpdate(){
    this.customerService.updateCustomerInfo(this.customer)
    this.edit=false
  }

}
