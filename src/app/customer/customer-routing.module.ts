import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartComponent } from './cart/cart.component';
import { CustomerComponent } from './customer.component';
import { ProfileComponent } from './profile/profile.component';
import { ViewproductComponent } from './viewproduct/viewproduct.component';

const routes: Routes = [
  {path: "", component: CustomerComponent,
    children : [
    {path: 'view-product', component: ViewproductComponent},
    {path: 'cart', component: CartComponent},
    {path: 'profile', component: ProfileComponent},
    {path: '', redirectTo: 'view-product', pathMatch: 'full'}
  ]
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
